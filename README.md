# ![LEGOS](docs/legos_logo.png) <br/> Lite Emulator of Grid Operations

 ## Software
[**Raspberry-Pi 3**](https://ubuntu.com/tutorials/how-to-install-ubuntu-on-your-raspberry-pi#1-overview) is used as a host to provide edge-cloud services in the form of ancillary services for energy management, power grid automation and building automation systems, and network connectivity to [**LEGOS**](https://git.rwth-aachen.de/acs/public/teaching/legos/concept) via WiFi hotspot. The software environment is composed by Ubuntu server (21.04) operating system and a cloud architecture based on the Docker middleware paradigm: each service runs in an isolated container and can communicate with other containers through standard APIs. This middleware layer allows to pull additional modules that will run in separate containers (eg.[Apache](https://httpd.apache.org/), [Mosquitto](https://mosquitto.org/), [FIWARE](https://www.fiware.org/)).

![architecture](docs/architecture.png)

## Configuration
1. [**Raspberry-pi:**](https://ubuntu.com/tutorials/how-to-install-ubuntu-on-your-raspberry-pi#1-overview) Raspberry-Pi 3 is used as a software platform in this LEGOS project.
2. [**Network:**](setup/network/network.md) used for connecting to the Linux server on Raspberry-Pi 3.
3. [**Docker:**](setup/docker-engine/docker-engine.md) is an open-source project that automates the deployment of applications inside software containers, by providing an additional layer of abstraction and automation of operating-system-level virtualization on Linux, Mac OS and Windows.in this project required images are pulled via docker such as:
    - [**Mosquitto:**](setup/mosquitto/mosquitto.md)
  Eclipse Mosquitto is an open source implementation of a server for versions 5, 3.1.1, and 3.1 of the MQTT protocol
    - [**Apache:**](setup/apache/apache.md) the Apache HTTP Server, colloquially called Apache, is a Web server application notable for playing a key role in the initial growth of the World Wide Web.
    - **FIWARE:**
	    - [**Mongodb:**](setup/mongo-db/mongo-db.md) is a free and open-source cross-platform document-oriented database program.
	    - [**Orion:**](setup/orion/orion.md) brings a curated framework of open source software platform components.
	    - [**Docker-Compose:**](setup/docker-compose/docker-compose.md) allows to link an Orion Context Broker container to a MongoDB container in a few minutes.
     
## Copyright

2021, Institute for Automation of Complex Power Systems, EONERC

## License

<a rel="license" href="LICENSE.md"><img alt="MIT License" style="border-width:0" src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/License_icon-mit-88x31-2.svg/88px-License_icon-mit-88x31-2.svg.png" /></a><br />This work is licensed under a <a rel="license" href="LICENSE.md">MIT License</a>.

## Funding
<a rel="funding" href="https://fismep.de/"><img alt="FISMEP" style="border-width:0" src="docs/fismep_logo.png" width="78" height="63"/></a><br />This work was supported by <a rel="fismep" href="https://fismep.de/">FIWARE for Smart Energy Platform</a> (FISMEP), a German project funded by the *Federal Ministry for Economic Affairs and Energy (BMWi)* and the *ERA-Net Smart Energy Systems* Programme under Grant 0350018A.

## Contact

[![EONERC ACS Logo](docs/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- [Dr. Carlo Guarnieri Calò Carducci](mailto:cguarnieri@eonerc.rwth-aachen.de)

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[E.ON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)
