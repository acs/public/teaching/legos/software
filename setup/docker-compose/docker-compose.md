# Docker Compose
Compose is a tool for defining and running multi-container Docker applications, using a YAML file to configure application’s services.

## Install
Install Docker Compose via apt.
```
sudo apt-get install docker-compose 
sudo docker-compose version
```

## Run
Start always the service before Orion.
```
sudo docker-compose up
```

Stop after using.
```
sudo docker-compose down
```