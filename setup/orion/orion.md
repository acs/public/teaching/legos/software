# Orion
Orion Context Broker allows to manage the entire lifecycle of context information including updates, queries, registrations and subscriptions. It is an NGSIv2 server implementation to manage context information and its availability. Using the [Orion Context Broker](https://fiware-orion.readthedocs.io/en/master/), allows to create context elements and manage them through updates and queries. In addition, it allows to subscribe to context information so when some condition occurs (e.g. the context elements have changed) a notification is received.

## Overview
Instructions are based on the FIWARE GE implementation for RaspberryPi provide by [Let's FIWARE](https://github.com/lets-fiware/fiware-pi).

## Pre-Install
### Add swap
Building Orion requires more memory than the installed RAM, therefore we need to create a suitable swap memory.
```
sudo fallocate -l 2G /swapfile
```

_In case of **fallocate** error use this command._
```
sudo dd if=/dev/zero of=/swapfile bs=1024 count=2097152
```

Set the file permissions to 600 to prevent regular users to write and read the file.
```
sudo chmod 600 /swapfile
```

Create a Linux swap area, activate the swap file and check the status.
```
sudo mkswap /swapfile
sudo swapon /swapfile
sudo swapon --show
sudo free -h
```

_To make the change permanent edit the file **/etc/fstab file**_.
```
sudo nano /etc/fstab
```
_and paste the following line_
```
/swapfile swap swap defaults 0 0
```

### Configure swap

On Ubuntu, the default swappiness value is set to 60. You can check the current value by typing the following command.
```
cat /proc/sys/vm/swappiness
```

While the swappiness value of 60 is OK for most Linux systems, for production servers, you may need to set a lower value.

For example, you can set the swappiness value to 10.
```
sudo sysctl vm.swappiness=10
```

To make this parameter persistent across reboots, append the following line to the **/etc/sysctl.conf** file:
```
vm.swappiness=10
```

The optimal swappiness value depends on your system workload and how the memory is being used. Adjust this parameter in small increments to find an optimal value.

### Remove swap
Deactivate the swap space.
```
sudo swapoff -v /swapfile
```

_If previously added, edit the file **/etc/fstab file**_.
```
sudo nano /etc/fstab
```
_and remove the following line_
```
/swapfile swap swap defaults 0 0
```

Remove the actual swapfile file.
```
sudo rm /swapfile
```

## Install
Clone fiware-pi repository.
```
git clone https://github.com/lets-fiware/fiware-pi.git
```

Build Orion.
```
cd fiware-pi/build/orion/orion-3.0.0
sudo ./build.sh
```

## Run
Start up Orion and mongodb with the default **or** [FISMEP](../fismep/fismep.md) **docker-compose.yml** file.
```
cd fiware-pi/build/orion/orion-3.0.0
sudo systemctl status mongod
sudo systemctl start mongod
sudo docker-compose up
```

Run the following command to confirm that Orion has been successfully built and running.
```
curl localhost:1026/version
```










