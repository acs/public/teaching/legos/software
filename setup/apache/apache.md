# Apache
The Apache HTTP Server Project is an effort to develop and maintain an open-source HTTP server for modern operating systems including UNIX and Windows.

## Install
Pull the Apache HTTP Server image.
```
docker pull httpd
```

Pull the LEGOS http server image.
```
mkdir ~/legos
cd ~/legos
git init
git remote add origin -f https://git.rwth-aachen.de/acs/public/teaching/legos/software.git
git config core.sparseCheckout true
echo setup/apache/docker_image/ >> .git/info/sparse-checkout
git read-tree -mu HEAD
git pull origin master
```

## Build
Build the LEGOS http server image.
```
cd ~/legos/setup/apache/docker_image
sudo docker build -t legos_http .
```

## Run
Execute the docker image in background on port 80.
For automatic restart of the service after reboot, add *--restart always* argument after the run command.
```
cd
sudo docker run -dit --name legos_http -p 80:80 legos_http
```
