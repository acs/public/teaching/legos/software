# MongoDB

Free and open-source cross-platform document-oriented database program.

## Overview
Use this tutorial to install MongoDB 4.4 Community Edition on LTS (long-term support) releases of Ubuntu Linux using the apt package manager.

## Install
Import the public key used by the package management system.
```
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
```

_In case of error **gnupg is not installed**, install gnupg and its required libraries._
```
sudo apt-get install gnupg
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
```

Create the list file **/etc/apt/sources.list.d/mongodb-org-4.4.list** for your version of Ubuntu.
```
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
```

Reload the local package database and install mongo-db.
```
sudo apt-get update
sudo apt-get install mongodb-org
```

_**Optional**. To prevent unintended package upgrades, you can pin the package at the currently installed version_
```
echo "mongodb-org hold" | sudo dpkg --set-selections
echo "mongodb-org-server hold" | sudo dpkg --set-selections
echo "mongodb-org-shell hold" | sudo dpkg --set-selections
echo "mongodb-org-mongos hold" | sudo dpkg --set-selections
echo "mongodb-org-tools hold" | sudo dpkg --set-selections
```

## Run

Configure mongo-db for automatic start after system reboot, start and check service status.
```
sudo systemctl enable mongod
sudo systemctl start mongod
sudo systemctl status mongod
```

_In case of error **Failed to start mongod.service: Unit mongod.service not found**, restart the service._
```
sudo systemctl daemon-reload
```

Start a mongo shell without any command-line options to connect to a mongod that is running on your localhost with default port 27017.
```
mongo
```

The log file is available in **/var/log/mongodb/mongod.log**.

Stop MongoDB if required.
```
sudo systemctl stop mongod
```