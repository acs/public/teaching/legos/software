# Network

The network configuration provides a WiFi Access Point (AP) to the IoT entities of LEGOS.

## Install/Configure

Install Network Manager
```
sudo apt update
sudo apt install network-manager
```

Disable cloud-init
```
sudo bash -c "echo 'network: {config: disabled}' > /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg"
```

Create a Netplan configuration
```
sudo nano /etc/netplan/10-my-config.yaml
```

Then add the following configuration
```
network:
  version: 2
  renderer: NetworkManager
  ethernets:
    eth0:
      dhcp4: true
      optional: true
  wifis:
    wlan0:
      dhcp4: true
      optional: true
      addresses: [192.168.1.1/24]
      gateway4: 192.168.1.1
      access-points:
        "LEGOS":
          password: "legos@acs"
          mode: ap
```

Apply the Netplan configuration
```
sudo netplan generate
sudo netplan apply
```

## WiFi driver
The latest WiFi driver supports only a limited number of clients, therefore we need to restore the older version [**7.45.18.0**](https://github.com/iiab/iiab/issues/823).

Backup the current driver.
```
cd /lib/firmware/brcm
sudo mv brcmfmac43455-sdio.bin brcmfmac43455-sdio.bin.bak
```

Download and rename the old driver.
```
sudo wget http://d.iiab.io/packages/brcmfmac43455-sdio.bin_2015-03-01_7.45.18.0_ub19.10.1
sudo mv brcmfmac43455-sdio.bin_2015-03-01_7.45.18.0_ub19.10.1 brcmfmac43455-sdio.bin
```

Verify the changes
```
ip addr show dev wlan0
```

Reboot to make the old driver operative.
