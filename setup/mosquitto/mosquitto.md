# Mosquitto
Eclipse Mosquitto is an open source (EPL/EDL licensed) message broker that implements the MQTT protocol.

## Install
Pull the Mosquitto image
```
docker pull eclipse-mosquitto
```

Pull the LEGOS mosquitto image.
```
mkdir ~/legos
cd ~/legos
git init
git remote add origin -f https://git.rwth-aachen.de/acs/public/teaching/legos/software.git
git config core.sparseCheckout true
echo setup/mosquitto/docker_image/ >> .git/info/sparse-checkout
git read-tree -mu HEAD
git pull origin master
```

## Configure
The broker is configured for authenticated access without encryption.
```
user: LEGOS
pass: legos@mqtt
```
### *Optional*
- To allow access without authentication, edit the configuration file and set `allow_anonymous true`
```
cd ~/legos/setup/mosquitto/docker_image/mosquitto/config
nano mosquitto.conf
```

- To add/change the authentication settings, edit the password file and add/change the entries as described in the reference documentation [[link]](https://mosquitto.org/man/mosquitto_passwd-1.html)
```
cd ~/legos/setup/mosquitto/docker_image/mosquitto/config
nano pwfile
```

## Build
Build the LEGOS mosquitto image.
```
cd ~/legos/setup/mosquitto/docker_image
sudo docker build -t legos_mqtt .
```

## Run
Execute the docker image in background on port 1883.
```
cd
sudo docker run -dit --name legos_mqtt -p 1883:1883 legos_mqtt
```
### *Optional*
For automatic restart of the service after reboot, add `--restart always` argument after the run command.
